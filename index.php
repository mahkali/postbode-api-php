<?php
/*
 * Postbode PHP API Client
 *
 * @package    Postbode.nu
 * @author     Mark Hameetman <mark@bureaupartners.nl>
 * @copyright  (c) 2014 Postbode B.V.
 * @version    2.0
 * @link       https://bitbucket.org/postbodenu/postbode-api-php
 */

include_once('classes/postbode.class.php');

?>